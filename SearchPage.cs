﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Reflection.PortableExecutable;
using System.Text;

namespace OnlineShopAutomationTest
{
    public class SearchPage
    {
        private readonly IWebDriver _driver;

        public SearchPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public string ProductName => "Faded Short Sleeve T-shirts";
        public IWebElement SearchField => _driver.FindElement(By.Name("search_query"));
        public IWebElement SearchButton => _driver.FindElement(By.Name("submit_search"));
        public IWebElement ProductDetailLink => _driver.FindElement(By.XPath("//ul[@class='product_list grid row']//a[@title='" + ProductName + "' and @class='product-name']"));
        public IWebElement CloseModalButton => _driver.FindElement(By.ClassName("cross"));
        public IWebElement CartProductLink => _driver.FindElement(By.LinkText(ProductName));
        public IWebElement OpenCartButton => _driver.FindElement(By.XPath("//a[@title='View my shopping cart']"));
        public IWebElement AddFromProductDetail => _driver.FindElement(By.Name("Submit"));
        public IWebElement AddFromSearch => _driver.FindElement(By.XPath("//a[@title='Add to cart']"));
        public Actions Action => new Actions(_driver);
    }
}
