﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using System;
using System.Threading;

namespace OnlineShopAutomationTest
{
    [TestFixture]
    public class TestAddToCart
    {
        private SearchPage _searchPage;
        private IWebDriver _driver;

        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            _driver.Navigate().GoToUrl("http://automationpractice.com/index.php");
            _searchPage = new SearchPage(_driver);
        }

        [Test]
        public void GIVEN_ProductNameToSearch_WHEN_AdddingToCartWithinTheProductDetails_THEN_AddToCart()
        {
            _searchPage.SearchField.SendKeys(_searchPage.ProductName);
            _searchPage.SearchButton.Click();
            _searchPage.ProductDetailLink.Click();

            _searchPage.AddFromProductDetail.Click();

            _searchPage.CloseModalButton.Click();
            _searchPage.OpenCartButton.Click();

            Assert.IsTrue(_searchPage.CartProductLink.Displayed, "Product not added to cart");
        }

        [Test]
        public void GIVEN_ProductNameToSearch_WHEN_AdddingToCartFromSearchResult_THEN_AddToCart()
        {

            _searchPage.SearchField.SendKeys(_searchPage.ProductName);
            _searchPage.SearchButton.Click();

            _searchPage.Action.MoveToElement(_searchPage.ProductDetailLink).Perform();
            
            _searchPage.AddFromSearch.Click();

            _searchPage.CloseModalButton.Click();
            _searchPage.OpenCartButton.Click();

            Assert.IsTrue(_searchPage.CartProductLink.Displayed, "Product not added to cart");
        }

        [TearDown]
        public void Dispose()
        {
            _driver.Dispose();
        }
    }
}
